#!/usr/bin/env python3
# encoding: utf-8

from waflib.Tools import waf_unit_test
from waflib.Build import BuildContext
import os
from contextlib import contextmanager


VERSION = "0.1"
APPNAME = "flux"

top = "."
out = "build"


def init(ctx):
    ctx.load("build_logs")


def options(ctx):
    ctx.load("compiler_cxx")
    ctx.load("gnu_dirs")
    ctx.load("waf_unit_test")
    ctx.add_option(
        "--skip-tests",
        action="store_true",
        default=False,
        help="dont build test binaries",
    )


def configure(ctx):
    ctx.env.CXXFLAGS = os.environ.get(
        "CXXFLAGS", "-g -O0 -std=c++2b -Wall -Wextra -Werror"
    ).split()
    ctx.load("compiler_cxx")
    ctx.load("gnu_dirs")
    ctx.load("boost")
    ctx.load("clang_compilation_database")
    ctx.load("waf_unit_test")
    ctx.check_cfg(package="spdlog", args=["--cflags", "--libs"])
    ctx.check_boost(lib='system program_options')
    ctx.msg("Used CXXFLAGS", " ".join(ctx.env.CXXFLAGS))
    ctx.msg("Used LINKFLAGS", " ".join(ctx.env.LINKFLAGS))
    ctx.msg("Used DEFINES", " ".join(ctx.env.DEFINES))


def build(ctx):
    ctx.env.INCLUDES = [ctx.path.abspath()]
    ctx.objects(
        source=ctx.path.ant_glob(["src/lib/**/*.(c|cpp)"]),
        target="SRCLIB",
        features="cxx",
        use=["BOOST", "SPDLOG"],
    )
    for suite in ctx.path.ant_glob(["src/bin/**/*.(c|cpp)"]):
        target, _ = suite.name.split(".")
        ctx(
            source=suite,
            target=target,
            vnum=VERSION,
            features="cxx cxxprogram",
            use=["SRCLIB", "BOOST", "SPDLOG"],
            lib=["pthread"],
        )

    # if not ctx.options.skip_tests:
    #     ctx(
    #         source=ctx.path.ant_glob(['test/test_main.cpp']),
    #         target='test_main',
    #         features='cxx',
    #         includes=['test'],
    #     )

    #     for suite in ctx.path.ant_glob(
    #         ['test/**/*.(c|cpp)'],
    #             excl=['test/test_main.cpp']):
    #         target, _ = suite.name.split('.')
    #         ctx(
    #             source=suite,
    #             target='ut_' + target,
    #             features='cxx cxxprogram test',
    #             use=['test_main', 'SRCLIB'],
    #             lib=['pthread'],
    #             install_path=None,
    #         )

    #     ctx.add_post_fun(waf_unit_test.summary)
    #     ctx.add_post_fun(waf_unit_test.set_exit_code)
