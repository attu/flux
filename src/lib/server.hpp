#pragma once

#include "src/lib/any_connector.hpp"

namespace flux {
struct server {
  static void run(boost::asio::io_context&,
                  std::string,
                  std::uint16_t,
                  AnyConnector);
  template <Connector C>
  static void run(boost::asio::io_context& ctx,
                  std::string host,
                  std::uint16_t port,
                  C c) {
    run(ctx, std::move(host), port, AnyConnector{std::move(c)});
  }
};
}  // namespace flux