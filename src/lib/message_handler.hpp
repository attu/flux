#pragma once
#include <boost/asio.hpp>
#include <cstddef>
#include <memory>
#include <optional>
#include <span>
#include <tuple>
#include <utility>
#include <variant>
#include <vector>

#include "src/lib/message.hpp"

namespace flux {
// using message = std::vector<std::byte>;

// using message_view = std::span<std::byte>;

using Request = byte_view;

using Response = message;

using Error = boost::system::error_code;

template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};

template <typename T, typename E>
using Result = std::variant<T, E>;

struct AnyHandler;

using Output =
    Result<std::tuple<std::optional<Response>, std::optional<AnyHandler>>,
           Error>;

template <typename A>
concept MessageHandler = std::semiregular<A> && std::swappable<A> &&
    requires(A a, Result<Request, Error> req) {
  {
    a.handle(req)
    } -> std::same_as<Result<std::tuple<std::optional<flux::Response>,
                                        std::optional<flux::AnyHandler>>,
                             Error>>;
};

struct AnyHandler {
 private:
  struct api_t {
    virtual ~api_t() = default;
    virtual Result<std::tuple<std::optional<flux::Response>,
                              std::optional<flux::AnyHandler>>,
                   Error>
        handle(Result<Request, Error>) = 0;
  };
  template <MessageHandler H>
  struct model_t;
  std::unique_ptr<api_t> impl;

 public:
  AnyHandler(AnyHandler&&) noexcept = default;
  AnyHandler& operator=(AnyHandler&&) noexcept = default;
  ~AnyHandler() noexcept = default;

  template <MessageHandler H>
  AnyHandler(H&&);

  Result<std::tuple<std::optional<flux::Response>,
                    std::optional<flux::AnyHandler>>,
         Error>
      handle(Result<Request, Error>);
};

template <MessageHandler H>
struct AnyHandler::model_t final : AnyHandler::api_t {
  explicit model_t(H&& h) : inner{std::forward<H>(h)} {}
  Result<std::tuple<std::optional<flux::Response>,
                    std::optional<flux::AnyHandler>>,
         Error>
  handle(Result<Request, Error> req) override {
    return inner.handle(req);
  }

 private:
  H inner;
};

template <MessageHandler H>
AnyHandler::AnyHandler(H&& h)
    : impl{std::make_unique<AnyHandler::model_t<H>>(std::forward<H>(h))} {}

}  // namespace flux