#pragma once
#include <boost/asio.hpp>
#include <memory>
#include "src/lib/message_handler.hpp"

namespace flux {
struct receiver {
  static void run(boost::asio::io_context&,
                  std::unique_ptr<boost::asio::ip::tcp::socket>,
                  AnyHandler,
                  std::uint16_t chunk_size);
};
}  // namespace flux