#include "src/lib/any_connector.hpp"

void flux::AnyConnector::on_connect(boost::asio::io_context& ctx,
                                    std::unique_ptr<flux::tcp::socket> socket) {
  impl->on_connect(ctx, std::move(socket));
}