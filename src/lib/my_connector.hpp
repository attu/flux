#pragma once
#include <boost/asio.hpp>
#include <memory>

namespace flux {
struct MyConnector {
  std::uint16_t chunk_size;
  std::uint32_t connects = 0;
  void on_connect(boost::asio::io_context&,
                  std::unique_ptr<boost::asio::ip::tcp::socket>);
};
}  // namespace flux