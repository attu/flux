#include "src/lib/other_handler.hpp"
#include <spdlog/spdlog.h>
#include "src/lib/my_handler.hpp"

flux::Output flux::OtherMessageHandler::handle(
    flux::Result<flux::Request, flux::Error> request) const {
  return std::visit(*this, request);
}

flux::Output flux::OtherMessageHandler::operator()(Error ec) const {
  spdlog::info("OtherMessageHandler {}:{} error", connection, count);
  return ec;
}

flux::Output flux::OtherMessageHandler::operator()(Request req) const {
  spdlog::info("OtherMessageHandler {}:{} request", connection, count);
  return std::make_tuple(
      Response(bytes(std::begin(req), std::end(req))),
      MyMessageHandler{.connection = connection, .count = count + 1});
}
