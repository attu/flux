#include "src/lib/args.hpp"

#include <boost/program_options.hpp>
#include <iostream>

namespace po = boost::program_options;

std::variant<flux::config::Args, int> flux::config::parse_args(int argc,
                                                               char* argv[]) {
  po::options_description desc("Allowed options");

  Args args;
  auto level = spdlog::level::to_string_view(args.logging.level);
  std::string log_level(std::begin(level), std::end(level));

  // clang-format off
  desc.add_options()
    ("help,h", "Show help message")
    ("host,H", po::value<std::string>(&args.server.host)->default_value(args.server.host), "Server host")
    ("port,p", po::value<std::uint16_t>(&args.server.port)->default_value(args.server.port), "Server port")
    ("log-level,l", po::value<std::string>(&log_level)->default_value(log_level), "Logging level")
    ("threads,t", po::value<std::int32_t>(&args.app.threads)->default_value(args.app.threads), "Application threads")
    ("chunk-size", po::value<std::uint16_t>(&args.server.chunk_size)->default_value(args.server.chunk_size), "Message chunk size")
  ;
  // clang-format on
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
  po::notify(vm);
  if (vm.count("help")) {
    std::cout << desc << std::endl;
    return EXIT_SUCCESS;
  }
  args.logging.level = spdlog::level::from_str(log_level);
  args.app.threads = (args.app.threads > 0)
                         ? args.app.threads
                         : std::thread::hardware_concurrency();
  args.server.chunk_size =
      (args.server.chunk_size > 0) ? args.server.chunk_size : 4 * (1 << 10);

  return args;
}