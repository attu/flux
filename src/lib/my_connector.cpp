#include "src/lib/my_connector.hpp"
#include <spdlog/spdlog.h>
#include "src/lib/my_handler.hpp"
#include "src/lib/receiver.hpp"

void flux::MyConnector::on_connect(
    boost::asio::io_context& ctx,
    std::unique_ptr<boost::asio::ip::tcp::socket> socket) {
  connects += 1;
  spdlog::info("MyConnector::on_connect {}", connects);
  receiver::run(ctx, std::move(socket),
                MyMessageHandler{.connection = connects, .count = 1},
                chunk_size);
}