#pragma once
#include "src/lib/message_handler.hpp"

namespace flux {
struct MyMessageHandler {
  std::uint32_t connection = 0;
  std::uint32_t count = 0;

  Output handle(Result<Request, Error>) const;
  Output operator()(Error) const;
  Output operator()(Request) const;
};
}