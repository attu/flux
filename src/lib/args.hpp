#pragma once

#include <spdlog/spdlog.h>

#include <string>
#include <thread>
#include <variant>

namespace flux::config {
struct Args {
  struct {
    std::string host = "0.0.0.0";
    std::uint16_t port = 0;
    std::uint16_t chunk_size = 4 * (1 << 10);
  } server;
  struct {
    spdlog::level::level_enum level = spdlog::level::info;
  } logging;
  struct {
    std::int32_t threads = std::thread::hardware_concurrency();
  } app;
};

std::variant<Args, int> parse_args(int, char*[]);
}  // namespace flux::config