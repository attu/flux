#pragma once

#include <cstdint>
#include <span>
#include <vector>

namespace flux {
using bytes = std::vector<std::byte>;
using byte_view = std::span<bytes::value_type const>;
class message : bytes {
 public:
  explicit message(bytes msg) noexcept : bytes(std::move(msg)) {}
  message(message const&) = delete;
  message(message&&) = default;
  message& operator=(message const&) = delete;
  message& operator=(message&&) = default;
  using bytes::begin;
  using bytes::data;
  using bytes::end;
  using bytes::size;

  operator byte_view() const noexcept { return byte_view(data(), size()); }
};
}  // namespace flux