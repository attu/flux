#pragma once
#include <boost/asio.hpp>
#include <memory>

namespace flux {
using tcp = boost::asio::ip::tcp;
template <typename A>
concept Connector = std::semiregular<A> && std::swappable<A> &&
    requires(A a,
             boost::asio::io_context& ctx,
             std::unique_ptr<tcp::socket> socket) {
  {a.on_connect(ctx, std::move(socket))};
};

struct AnyConnector {
 private:
  struct api_t {
    virtual ~api_t() = default;
    virtual void on_connect(boost::asio::io_context&,
                            std::unique_ptr<tcp::socket>) = 0;
  };

  template <Connector A>
  struct model_t final : api_t {
    explicit model_t(A&& a) : inner{std::forward<A>(a)} {}
    void on_connect(boost::asio::io_context& ctx,
                    std::unique_ptr<tcp::socket> socket) override {
      inner.on_connect(ctx, std::move(socket));
    }

   private:
    A inner;
  };

  std::unique_ptr<api_t> impl;

 public:
  AnyConnector(AnyConnector&&) noexcept = default;
  AnyConnector& operator=(AnyConnector&&) noexcept = default;
  ~AnyConnector() noexcept = default;

  template <Connector A>
  AnyConnector(A&& a)
      : impl{std::make_unique<model_t<A>>(std::forward<A>(a))} {}

  void on_connect(boost::asio::io_context&, std::unique_ptr<tcp::socket>);
};
}  // namespace flux