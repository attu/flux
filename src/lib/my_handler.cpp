#include "src/lib/my_handler.hpp"
#include <spdlog/spdlog.h>
#include "src/lib/other_handler.hpp"

flux::Output flux::MyMessageHandler::handle(
    Result<Request, Error> request) const {
  return std::visit(*this, request);
}

flux::Output flux::MyMessageHandler::operator()(Error ec) const {
  spdlog::info("MyMessageHandler {}:{} error", connection, count);
  return ec;
}

flux::Output flux::MyMessageHandler::operator()(Request req) const {
  spdlog::info("MyMessageHandler {}:{} request", connection, count);
  return std::make_tuple(
      Response(bytes(std::begin(req), std::end(req))),
      OtherMessageHandler{.connection = connection, .count = count + 1});
}