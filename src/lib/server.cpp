#include "src/lib/server.hpp"
#include <spdlog/spdlog.h>

namespace {
struct impl {
  explicit impl(boost::asio::io_context& ctx, flux::AnyConnector connector)
      : ctx{ctx}, connector{std::move(connector)} {}
  boost::asio::io_context& ctx;
  flux::tcp::acceptor acceptor{ctx};
  flux::AnyConnector connector;
};

void do_accept(std::unique_ptr<impl> state) {
  auto socket = std::make_unique<flux::tcp::socket>(state->ctx);
  auto& socket_ref = *socket;
  auto& acceptor = state->acceptor;

  auto callback = [state{std::move(state)},
                   socket{std::move(socket)}](auto ec) mutable {
    if (!ec) {
      state->connector.on_connect(state->ctx, std::move(socket));
    }

    do_accept(std::move(state));
  };

  acceptor.async_accept(socket_ref, std::move(callback));
}
}  // namespace

void flux::server::run(boost::asio::io_context& ctx,
                       std::string host,
                       std::uint16_t port,
                       flux::AnyConnector connector) {
  tcp::endpoint endpoint(boost::asio::ip::make_address(host), port);
  auto state = std::make_unique<impl>(ctx, std::move(connector));
  state->acceptor.open(endpoint.protocol());
  state->acceptor.set_option(boost::asio::socket_base::reuse_address(true));
  state->acceptor.bind(endpoint);
  state->acceptor.listen(boost::asio::socket_base::max_listen_connections);
  auto local_address = state->acceptor.local_endpoint();
  spdlog::info("local_address {}:{}", local_address.address().to_string(),
               local_address.port());
  do_accept(std::move(state));
}