#include "src/lib/message_handler.hpp"

flux::Result<
    std::tuple<std::optional<flux::Response>, std::optional<flux::AnyHandler>>,
    flux::Error>
flux::AnyHandler::handle(Result<Request, Error> req) {
  return impl->handle(req);
}