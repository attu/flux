#include "src/lib/receiver.hpp"
#include <spdlog/spdlog.h>
#include <cstddef>
#include "src/lib/message_handler.hpp"

namespace {
struct impl {
  impl(boost::asio::io_context& ctx,
       boost::asio::ip::tcp::socket socket,
       flux::AnyHandler handler,
       std::uint16_t chunk_size)
      : strand{ctx},
        socket{std::move(socket)},
        handler{std::move(handler)},
        data{chunk_size} {}
  boost::asio::io_context::strand strand;
  boost::asio::ip::tcp::socket socket;
  flux::AnyHandler handler;
  std::vector<std::byte> data{4 * (1 << 10)};
};

void do_receive(std::unique_ptr<impl>);
void do_send(std::unique_ptr<impl>,
             flux::message,
             std::optional<flux::AnyHandler>);

auto const make_visitor = [](std::unique_ptr<impl> state) {
  return flux::overloaded{
      [](flux::Error) -> void { spdlog::info("Exiting due to error..."); },
      [state{std::move(state)}](
          std::tuple<std::optional<flux::Response>,
                     std::optional<flux::AnyHandler>>&& tuple) mutable -> void {
        spdlog::info("sending response");
        auto [resp, handler] = std::move(tuple);
        if (resp) {
          return do_send(std::move(state), std::move(*resp),
                         std::move(handler));
        }
        if (handler) {
          state->handler = std::move(*handler);
          do_receive(std::move(state));
        }
      }};
};

void do_send(std::unique_ptr<impl> state,
             flux::message msg,
             std::optional<flux::AnyHandler> handler) {
  auto& socket = state->socket;
  auto buffer = boost::asio::buffer(std::data(msg), std::size(msg));
  auto callback = [msg{std::move(msg)}, state{std::move(state)},
                   handler{std::move(handler)}](auto ec, auto) mutable {
    if (ec) {
      auto ret = state->handler.handle(ec);
      std::visit(make_visitor(std::move(state)), std::move(ret));
    } else if (handler) {
      state->handler = std::move(*handler);
      do_receive(std::move(state));
    }
  };
  boost::asio::async_write(socket, buffer, std::move(callback));
}

void do_receive(std::unique_ptr<impl> state) {
  auto& strand = state->strand;
  auto& socket = state->socket;
  auto& data = state->data;
  auto callback = [state{std::move(state)}](auto ec, auto len) mutable {
    spdlog::info("got message");
    auto request = [&] -> flux::Result<flux::Request, flux::Error> {
      if (ec) return ec;
      return flux::Request(std::begin(state->data),
                           std::next(std::begin(state->data), len));
    }();
    auto resp = state->handler.handle(request);

    std::visit(make_visitor(std::move(state)), std::move(resp));
  };
  socket.async_read_some(
      boost::asio::buffer(std::data(data), std::size(data)),
      boost::asio::bind_executor(strand, std::move(callback)));
}
}  // namespace

void flux::receiver::run(boost::asio::io_context& ctx,
                         std::unique_ptr<boost::asio::ip::tcp::socket> socket,
                         AnyHandler h,
                         std::uint16_t chunk_size) {
  do_receive(std::make_unique<impl>(ctx, std::move(*socket), std::move(h),
                                    chunk_size));
}