#include <spdlog/spdlog.h>
#include <boost/asio.hpp>
#include <thread>
#include "src/lib/args.hpp"
#include "src/lib/my_connector.hpp"
#include "src/lib/server.hpp"

int main(int argc, char* argv[]) {
  auto const args = flux::config::parse_args(argc, argv);
  if (std::holds_alternative<int>(args)) {
    return std::get<int>(args);
  }
  auto const& conf = std::get<flux::config::Args>(args);
  spdlog::set_level(conf.logging.level);

  spdlog::debug("running {} threads", conf.app.threads);
  spdlog::debug("chunk_size {:08X}", conf.server.chunk_size);
  boost::asio::io_context context{conf.app.threads};
  boost::asio::signal_set signals{context, SIGTERM};
  signals.async_wait([&context](auto, auto signo) {
    spdlog::info("got signal {}, stopping io_context", signo);
    context.stop();
  });

  flux::server::run(context, conf.server.host, conf.server.port,
                    flux::MyConnector{.chunk_size = conf.server.chunk_size});

  std::vector<std::jthread> workers;
  std::generate_n(
      std::back_inserter(workers), conf.app.threads - 1,
      [&context] { return std::jthread{[&context] { context.run(); }}; });
  context.run();

  spdlog::info("bye, bye");
  return 0;
}
